/* Copyright (C) 2018 Joshua Mallad. No rights reserved.      
   log.h - 5-level logging interface */

#pragma once

/**
   @file
*/

#include <stdio.h>
#include <stdint.h>

#define DEBUG 0
#define INFO 1
#define WARNING 2
#define ERROR 3
#define FATAL 4

#define logs(_file, _level, _msg, ...) \
  _logs(_file, _level, FILENAME, FUNCTION, _msg, ##__VA_ARGS__)
#define log_debug(_msg, ...) logs(debugfp, DEBUG, _msg, ##__VA_ARGS__)
#define log_info(_msg, ...) logs(stderr, INFO, _msg, ##__VA_ARGS__)
#define log_warning(_msg, ...) logs(stderr, WARNING, _msg, ##__VA_ARGS__)
#define log_error(_msg, ...) logs(stderr, ERROR, _msg, ##__VA_ARGS__)
#define log_fatal(_msg, ...) logs(stderr, FATAL, _msg, ##__VA_ARGS__)

void log_levelstr(char* levelstr, uint8_t level, size_t len);
void _logs(FILE* fp, uint8_t level, const char* filename, const char* function, char* fmt, ...);

extern FILE* debugfp;
extern FILE* stderr;

