#pragma once
#include <stdatomic.h>
#include <stdint.h>

struct spinlock;

/* Allocate a new spinlock structure. */
int new_spinlock(struct spinlock** p);
/* Attempt to acquire a lock for writing. Return 0 if successful. */
int spin_write_lock(struct spinlock* p);
/* Attempt to acquire a lock for writing. Immediately return 1 if unable
   to acquire the lock on the first try. Return 0 if successful. */
int spin_try_write_lock(struct spinlock* p);
/* Release a write lock. */
int spin_write_unlock(struct spinlock* p);
/* Attempt to acquire a lock for reading. If successful, increment the reader  
   count, release the lock, and return 0. */
int spin_read_lock(struct spinlock* p);
/* Attempt to acquire a lock for reading. Immediately return 1 if unable
   to acquire the lock on the first try. Return 0 if successful. */
int spin_try_read_lock(struct spinlock* p);
/* Release a lock acquired for reading by decrementing the readers count. */
int spin_read_unlock(struct spinlock* p);
/* Free a spinlock */
void free_spinlock(struct spinlock* p);
