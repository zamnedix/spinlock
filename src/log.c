#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include "log.h"

/* Copyright (C) 2018 Joshua Mallad. No rights reserved.      
   log.c - 5-level logging interface */

/**
   @file
   @brief Logging utilities
*/

/**
 * Convert a log level to a human-readable string.
 * @param levelstr Destination output. Should be at least 8 bytes in size.
 * @param level Log level {DEBUG, INFO, WARNING, ERROR, FATAL}
 * @param len Length of levelstr buffer
 */
void log_levelstr(char* levelstr, uint8_t level, size_t len) {
  switch(level) {
  case DEBUG: strncpy(levelstr, "debug", len); break;
  case INFO: strncpy(levelstr, "info", len); break;
  case WARNING: strncpy(levelstr, "warning", len); break;
  case ERROR: strncpy(levelstr, "error", len); break;
  case FATAL: strncpy(levelstr, "fatal", len); break;
  default: strncpy(levelstr, "log", len); break;
  }
}

/**
 * Write a message to a log
 * @param fp Log file to use {stderr, debug, ...}
 * @param level Log level {DEBUG, INFO, WARNING, ERROR, FATAL}
 * @param filename Source filename of invoking function
 * @param function Name of invoking function
 * @param fmt Log message with optional format specifiers
 * @param ... Optional variables for formats
 */
void _logs(FILE* fp, uint8_t level, const char* filename, const char* function, char* fmt, ...) {
	va_list ap;
	char buf[65536] = {0};
	char levelstr[8] = {0};
	char timestr[64] = {0};
	char datestr[128] = {0};
	struct timespec current_time = {0};
	struct tm* current_time_tm = NULL;
	if (!fp || !filename || !function || !fmt) {
		return;
	}	
	if (!(timespec_get(&current_time, TIME_UTC))) {
		fprintf(stderr, "[error] log.c: timespec_get failed: %s",
			strerror(errno));
	}
	else {
		current_time_tm = localtime(&(current_time.tv_sec));
		if (!current_time_tm) {
			fprintf(stderr, "[error] log.c: localtime failed: %s",
				strerror(errno));
		}
	}
	strftime(timestr, 64, "%H:%M:%S", current_time_tm);
	strftime(datestr, 128, "%b %d %Y", current_time_tm);

	log_levelstr(levelstr, level, 8);

	va_start(ap, fmt);
	vsnprintf(buf, 65536, fmt, ap);
	va_end(ap);

	fprintf(fp, "[%s:%05lu %s] [%s] %s: %s: %s\n",
		timestr,
		(unsigned long)(current_time.tv_nsec / 10000),
		datestr, levelstr, filename, function, buf);
}

